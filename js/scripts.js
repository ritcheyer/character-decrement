/*

Original:          http://jsfiddle.net/ritcheyer/KuN8u/
Forked by @niccai: http://jsfiddle.net/niccai/D76CV/
Forked again by @ritcheyer

Changes:
- combined all vars
- reordered vars so maxlength can be set by data attrib of input field

*/

function updateCountdown(input) {
	var $input = input,
		$countDisplay = $input.next(),
		inputMax = $input.data('maxlength'),
		counter = $input.val().length,
		classSet = 'warn whoa ludicrous almost';

	counter = inputMax - counter;

	if (counter > 20) {
		// normal
		$countDisplay.removeClass(classSet);
	} else {
		if (counter < 10) {
			if (counter < 5) {
				if (counter < 0) {
					// ludicrous
					$countDisplay.removeClass(classSet).addClass('ludicrous');
				} else {
					// whoa
					$countDisplay.removeClass(classSet).addClass('whoa');
				}
			} else {
				// warn
				$countDisplay.removeClass(classSet).addClass('warn');
			}
		} else {
			// almost
			$countDisplay.removeClass(classSet).addClass('almost');
		}
	}

	$countDisplay.text(counter);
}

$('document').ready(function() {
	
	$('input[type=text]').focus(function() {

		if ($(this).data('countdown')) {
			updateCountdown($(this));
			$(this).next('span').fadeIn(250);
		}
	}).change(function() {
		updateCountdown($(this))
	}).keyup(function() {
		updateCountdown($(this))
	});
});